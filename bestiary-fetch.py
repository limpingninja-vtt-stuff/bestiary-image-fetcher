import os, sys, requests
from string import ascii_uppercase
from bs4 import BeautifulSoup

baseurl = 'https://2e.aonprd.com/'
searchurl = 'Monsters.aspx'
outpath = './output'

# Format {name:id}
bestiary = {}
uncollected = []

mono    = False       # If color is a problem for your eyes set to True
CEND    = '\33[0m'
CRED    = '\33[31m'
CGREEN  = '\33[32m'
CYELLOW = '\33[33m'
CVIOLET = '\33[35m'
CWHITE  = '\33[37m'
CCYAN   = '\33[1;36m'

MSG_MAP = { 'info': CWHITE, 'dbug': CGREEN, 'fail': CRED, 'warn': CYELLOW, 'base': CVIOLET, 'crit': CCYAN }

def output(level, msg, decorate=True):
    if(level not in MSG_MAP.keys()):
        output('fail', 'Not a valid output type!')
        return
    if(decorate):
        print(f'{("" if mono else MSG_MAP[level])}{level.upper()} {("" if mono else CEND)}| {msg}')
    else:
        print(f'{("" if mono else MSG_MAP[level])}{msg}')

def fail(msg):
    output('fail',msg)

def write(msg):
    output('info',msg)

def is_downloadable(url):
    """
    Does the url contain a downloadable resource
    """
    h = requests.head(url, allow_redirects=True)
    header = h.headers
    content_type = header.get('content-type')
    if 'text' in content_type.lower():
        return False
    if 'html' in content_type.lower():
        return False
    return True

def fetch_thumbnail(creature):
    """
    Given the creature name, fetch the URL by ID where the creature
    resides, parse the html and grab the first img.thumbnail we see
    and download the image into the output directory.
    """
    write(f'Parsing Archives of Nethys listing for {creature} with ID {bestiary[creature]}')
    page = requests.get(f'{baseurl}{searchurl}?ID={bestiary[creature]}')
    stew = BeautifulSoup(page.content, 'html.parser')

    thumbnail = stew.find('img',{'class':'thumbnail'})
    if thumbnail:
        try:
            imgurl = f'{baseurl}{thumbnail.get("src")}'
            imgext = imgurl.split('.')[-1]
            g = requests.get(imgurl, allow_redirects=False)
            localfile = f"{creature}.{imgext}"
            open(f"{outpath}/{localfile}", 'wb').write(g.content)
            output("crit",f'Image found, retrieved locally to {localfile}')
        except:
            e = sys.exc_info()[0]
            fail(e)
    else:
        output("warn", f'No image found for {creature} with ID {bestiary[creature]}')
        uncollected.append(creature)
    return


def main():
    output("base", "Pathfinder AON Bestiary -> Images Fetch", False)
    output("info", f"{'-'*39}", False)

    if not os.path.exists(outpath):
        try:
            output('dbug',f'Local output path {outpath} does not exist, creating')
            os.mkdir(outpath)
        except:
            e = sys.exc_info()[0]
            fail(e)

    downFiles = os.listdir(outpath)
    
    # Main loop to scrape monster names and links
    #  write("Retrieving the bestiary page from Archives of Nethys.")
    # page = requests.get(f'{baseurl}{searchurl}?Letter=All')
    # write("Parsing HTML from Archives of Nethys.")
    # stew = BeautifulSoup(page.content, 'html.parser')
    #
    # table = stew.find('table') #assumed only table
    # rows = table.find_all('tr')

    if not os.path.exists('./RadGridExport.csv'):
        fail("RadGridExport.csv needs to be located in the directory you run in.")
        print("\n\n"
		" To get a copy of this export file you will need to visit the Archives\n"
		" of Nethys Monster-All page and export the file. Use this link to find\n"
		" the right table:\n\n"
		"   https://2e.aonprd.com/Monsters.aspx?Letter=All\n\n"
		" On the right hand side at the top of the table in the 'Level' column\n"
		" header this is a very small square icon with 'csv' inside of it. Click\n"
		" on that and save the file to the directory you run this script from.\n\n");
        exit(1)

    with open("./RadGridExport.csv") as f:
        content = f.readlines()
    
    content = [x.strip() for x in content] 
    
    write('Generating bestiary dictionary')
    count = 0
    for row in content:
        if count == 0:
            count = 1
            continue
        count = count + 1
        data = row.split(',')[0].replace('""', '')
        soup = BeautifulSoup(data, 'html.parser')
        anchor = soup.find('a')
        link = anchor.get('href')
        name = anchor.get_text()
        bestiary[name] = link.split('=')[-1]
        
    write(f'Found {count} items and added them to the bestiary!')
        
    #for row in rows[1:]:
    #    anchor = row.find('a')
    #    if anchor:
    #        link = anchor.get('href')
    #        name = anchor.get_text()
    #        bestiary[name] = link.split('=')[-1]

    write('Bestiary Parsing:')
    counter = 0
    for creature in bestiary.keys():
        if creature+".png" in downFiles:
            output("info", f'An image for {creature} already exists, skipping.')
        else:
            fetch_thumbnail(creature)

    write("Process completed!")
    if(len(uncollected) > 0):
        output('warn',f'The following items had no accompanying images:')
        counter = 1
        row = ''
        for item in uncollected:
            row += f'{item:30}'
            if(counter % 3):
                counter += 1
                row += ' | '
            else:
                output('warn', row)
                row = ''
                counter = 1
        if row:
            output('warn', row)
            
main()

