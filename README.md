### *** _*Please note, this is for personal use only and could take quite some time to run*_

![Example Image](https://i.imgur.com/k93Z60R.png)

# NOTICE
Due to changes in AoN the script was broken, it currently has a temporary patch in until I decide how to proceed. Right now the solution when running this
script is to use the RadGridExport.csv file downloaded from Archives of Nethys and placed into the same directory as the script.

To get a copy of this export file you will need to visit the Archives of Nethys Monster-All page and export the file. Use this link to find the right table:

[https://2e.aonprd.com/Monsters.aspx?Letter=All](https://2e.aonprd.com/Monsters.aspx?Letter=All)

On the right hand side at the top of the table in the 'Level' column  header this is a very small square icon with 'csv' inside of it. Click on that and save the file to the directory you run this script from.


# Info
This snippet scrapes (by default) Archives of Nethys (2e.aonprd.com) to retrieve a list of creatures, then parses the webpage entry for each creature to pull out the image and store it locally. The intent is to be able to take this output directory and upload it into a personal VTT instance to link to the Bestiary entries easily.

# Usage
This was written with Python3 and there still may be some rough edges in the code, but given that it isn't a production running script, caveat emptor - especially at $0.

## Install dependencies:

```
pip3 install requests
pip3 install bs4
```

## Running 
After that it is simply a matter of adding the RadGridExplort.csv to the directory (**see notice above**) and running the script:

```
python3 bestiary-fetch.py
```

## Errata 
- By default it will automatically create a subdirectory *output* and save all the images to that. If you want to change the location saved, then modify the `outpath` variable on line 7.
- As an advanced user, you could modify the baseurl and point it to standard aeonprd, but you need to validate all the url pathing is correct. It **should** work.
- During the process of downloading the images it will provide you messages with the following categories:
  - CRIT: This is a critical success, obviously.
  - FAIL: This is a failure, just like it says.
  - INFO: Your standard run of the mil info message. 
  - WARN: Any kinds of warnings that may crop up.

The script does consider 'misses' as warnings, at the end of the script it will print out a list of all creatures that DID NOT have any images associated with them. If you find any bugs as you run it let me know.

## FAQ

**This is cool, but how the hell do I get these attached?**
Good question, the simple basis of this is that you want to copy all of the image files into a path in your VTT. Once you do that you will want to run a module to map the images to your chosen compendium/monster database.

This is an example if you were using:

1. Run this script to download all of the images.
2. Copy the images into a path in your VTT Installation.
3. Install a module that maps images to entries: Compendium Image Mapper: [Compendium Image Mapper](https://gitlab.com/Wilco7302/compendium-image-mapper)
4. Point the Compendium Mapper to the image path you copied the images, and the compendium you want to map then click 'Start Mapping'
  - Leave all of the settings as they are, automatch at 90% and manual map above 75%.

**My images keep disappearing from the Compendium**
This is because you are probably updating a compendium added by a module or a system. Everytime they update you will lose your images. You can easily rerun the mapper or you can create your own Compendium at the world level to use or a shared Compendium.

Steps for creating a Shared Compendium are here: [How to create a tiny module for Shared Content](https://www.reddit.com/r/FoundryVTT/comments/fvw3c7/how_to_create_a_tiny_module_for_shared_content/)

Further in that thread they have a Macro that can be used to copy all creatures from the System (or other) Compendium into your own Compendium. Here is the direct link: [Macro for compendium copying](https://www.reddit.com/r/FoundryVTT/comments/fvw3c7/how_to_create_a_tiny_module_for_shared_content/fvqfzb8?utm_source=share&utm_medium=web2x)

**[anything else other than bugs or enhancement questions]**
- If you have a BUG, submit an ISSUE
- If you have an enhancement, submit a PULL REQUEST

If you have questions about how to use Python, please visit [Python.org Website](https://www.python.org/)!


